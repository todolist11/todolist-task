﻿using Microsoft.EntityFrameworkCore;
using TodoList.Task.Infrastructure;

namespace TodoList.Api.Extensions
{
    internal static class MigratesExtensions
    {
        public static async System.Threading.Tasks.Task AutoMigrate(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var dataContext = scope.ServiceProvider.GetRequiredService<TasksContext>();
                await dataContext.Database.MigrateAsync();
            }
        }
    }
}
