#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["src/TodoList.Task.Api/TodoList.Task.Api.csproj", "src/TodoList.Task.Api/"]
COPY ["src/TodoList.Task.Application/TodoList.Task.Application.csproj", "src/TodoList.Task.Application/"]
COPY ["src/TodoList.Task.Domain/TodoList.Task.Domain.csproj", "src/TodoList.Task.Domain/"]
COPY ["src/TodoList.Task.Infrastructure/TodoList.Task.Infrastructure.csproj", "src/TodoList.Task.Infrastructure/"]
RUN dotnet restore "src/TodoList.Task.Api/TodoList.Task.Api.csproj"
COPY . .
WORKDIR "/src/src/TodoList.Task.Api"
RUN dotnet build "TodoList.Task.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "TodoList.Task.Api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "TodoList.Task.Api.dll"]