﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using TodoList.Task.Domain.Abstractions;
using TodoList.Task.Domain.Entities.TaskAggregate;

namespace TodoList.Task.Infrastructure
{
    public class TasksContext : DbContext, IUnitOfWork
    {
        public TasksContext(DbContextOptions<TasksContext> options)
           : base(options)
        {
        }

        public DbSet<Domain.Entities.TaskAggregate.Task> Tasks { get; set; }

        public DbSet<SubTask> SubTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            await SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
