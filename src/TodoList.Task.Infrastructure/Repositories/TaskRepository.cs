﻿using Microsoft.EntityFrameworkCore;
using TodoList.Task.Domain.Entities.TaskAggregate;
using TodoList.Task.Infrastructure.Abstractions;

namespace TodoList.Task.Infrastructure.Repositories
{
    /// <inheritdoc/>
    internal class TaskRepository : RepositoryBase<Domain.Entities.TaskAggregate.Task>, ITaskRepository
    {
        private readonly TasksContext _context;

        public TaskRepository(TasksContext context)
           : base(context)
        {
        }

        /// <inheritdoc/>
        protected override Task<Domain.Entities.TaskAggregate.Task> GetEntityAsync(int id, int userId, CancellationToken cancellationToken = default)
        {
            return _context
                .Tasks
                .Include(x => x.SubTasks)
                .Where(x => x.UserId == userId)
                .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }
    }
}
