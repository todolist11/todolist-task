﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Task.Infrastructure.Exceptions
{
    public class InfrastrcutureException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InfrastrcutureException"/> class
        /// </summary>
        public InfrastrcutureException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InfrastrcutureException"/> class
        /// </summary>
        /// <param name="message">Сообщение</param>
        public InfrastrcutureException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InfrastrcutureException"/> class
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="innerException"><see cref="Exception"/></param>
        public InfrastrcutureException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InfrastrcutureException"/> class
        /// </summary>
        /// <param name="info"><see cref="SerializationInfo"/></param>
        /// <param name="context"><see cref="StreamingContext"/></param>
        protected InfrastrcutureException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
