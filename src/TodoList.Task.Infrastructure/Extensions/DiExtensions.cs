﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TodoList.Task.Infrastructure;

namespace TodoList.Infrastructure.Extensions
{
    /// <summary>
    /// Расширения подключения зависимостей
    /// </summary>
    public static class DiExtensions
    {
        /// <summary>
        /// Подключение зависимостей Infrastructure
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        /// <param name="configuration"></param>
        /// <returns><see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddInfrastructureLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TasksContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("Db"), builder =>
                {
                    builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
                }));

            services.AddRepositories();

            return services;
        }

        private static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services;
        }
    }
}
