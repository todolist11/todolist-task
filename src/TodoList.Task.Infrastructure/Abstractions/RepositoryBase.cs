﻿using Microsoft.EntityFrameworkCore;
using TodoList.Task.Domain.Abstractions;
using TodoList.Task.Infrastructure.Exceptions;

namespace TodoList.Task.Infrastructure.Abstractions
{
    /// <summary>
    /// Базовая реализация методов репозитория
    /// </summary>
    /// <typeparam name="T"><see cref="IAggregateRoot"/></typeparam>
    internal abstract class RepositoryBase<T> : IRepository<T>
        where T : BaseEntity, IAggregateRoot
    {
        private readonly TasksContext _context;

        /// <summary>
        /// <see cref="IUnitOfWork"/>
        /// </summary>
        public IUnitOfWork UnitOfWork => _context;

        /// <summary>
        /// <see cref="DbSet{TEntity}"/>
        /// </summary>
        protected DbSet<T> Set => _context.Set<T>();

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{T}"/> class
        /// </summary>
        /// <param name="context"><see cref="AppContext"/></param>
        protected RepositoryBase(TasksContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        /// <inheritdoc/>
        public T Add(T entity)
        {
            return _context
                .Set<T>()
                .Add(entity)
                .Entity;
        }

        /// <inheritdoc/>
        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        /// <inheritdoc/>
        public async Task<T> GetAsync(int id, int userId, CancellationToken cancellationToken = default)
        {
            return (await GetEntityAsync(id, userId, cancellationToken)) ?? throw new NotFoundInfrastructureException(id);
        }

        /// <summary>
        /// Запрос получения сущности по ID
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <param name="userId"></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns>Сущность</returns>
        protected virtual Task<T> GetEntityAsync(int id, int userId, CancellationToken cancellationToken = default)
        {
            return _context
                .Set<T>()
                .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }
    }
    }
