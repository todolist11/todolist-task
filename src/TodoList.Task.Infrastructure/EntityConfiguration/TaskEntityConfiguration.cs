﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Task.Infrastructure.EntityConfiguration
{
    internal class TaskEntityConfiguration : IEntityTypeConfiguration<Domain.Entities.TaskAggregate.Task>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.TaskAggregate.Task> builder)
        {
            builder
                .ToTable("Tasks");

            builder
               .HasKey(d => d.Id);

            builder
                .Property(d => d.Title)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .Property(d => d.Description)
                .HasMaxLength(600);

            builder
                .Property(d => d.EndDate)
                .IsRequired();

            builder
               .Property(d => d.Status)
               .IsRequired();

            builder
                .Property(d => d.UserId)
                .IsRequired();
        }
    }
}
