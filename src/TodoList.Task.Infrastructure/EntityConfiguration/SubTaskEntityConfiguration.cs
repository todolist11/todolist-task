﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TodoList.Task.Domain.Entities.TaskAggregate;

namespace TodoList.Task.Infrastructure.EntityConfiguration
{
    internal class SubTaskEntityConfiguration : IEntityTypeConfiguration<SubTask>
    {
        public void Configure(EntityTypeBuilder<SubTask> builder)
        {
            builder
                .ToTable("SubTasks");

            builder
                .HasKey(d => d.Id);

            builder
                .Property(d => d.Title)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .Property(d => d.Description)
                .HasMaxLength(600);

            builder
                .Property(d => d.Status)
                .IsRequired();

            builder
                .Property(d => d.TaskId)
                .IsRequired();

            builder
                .HasOne(d => d.Task)
                .WithMany(d => d.SubTasks)
                .HasForeignKey(d => d.TaskId);
        }
    }
}
