﻿using TodoList.Domain.Exceptions;

namespace TodoList.Task.Domain
{
    /// <summary>
    /// Класс с базовыми проверками
    /// </summary>
    internal class Guard
    {
        /// <summary>
        /// Кинуть исключение если отсутствует значение
        /// </summary>
        /// <typeparam name="T">Значимый тип переменной</typeparam>
        /// <param name="obj">Переменная</param>
        /// <param name="propName">Имя переменной</param>
        public static void ThrowIfNull<T>(T? obj, string propName)
            where T : struct
        {
            if (obj == null)
            {
                throw new DomainException($"Значение {propName} не заполнено");
            }
        }

        /// <summary>
        /// Кинуть исключение если отсутствует значение
        /// </summary>
        /// <typeparam name="T">Ссылочный тип переменной</typeparam>
        /// <param name="obj">Переменная</param>
        /// <param name="propName">Имя переменной</param>
        public static void ThrowIfNull<T>(T obj, string propName)
            where T : class
        {
            if (obj == null)
            {
                throw new DomainException($"Значение {propName} не заполнено");
            }
        }

        /// <summary>
        /// Кинуть исключение если отсутствует значение
        /// </summary>
        /// <param name="obj">Строка</param>
        /// <param name="propName">Имя переменной строки</param>
        public static void ThrowIfNullOrWhiteSpace(string obj, string propName)
        {
            if (string.IsNullOrWhiteSpace(obj))
            {
                throw new DomainException($"Значение {propName} не заполнено");
            }
        }

        /// <summary>
        /// Кинуть исключение если превышена длина строки
        /// </summary>
        /// <param name="obj">Строка</param>
        /// <param name="propName">Имя переменной строки</param>
        /// <param name="length">Максимальная длина</param>
        public static void ThrowIfLengthExceeded(string obj, string propName, int length)
        {
            if (string.IsNullOrEmpty(obj))
            {
                return;
            }

            if (obj.Length > length)
            {
                throw new DomainException($"Значение {propName} превышает возможную длину в {length} символов");
            }
        }

        /// <summary>
        /// Кинуть исключение если значение меньше минимально возможного
        /// </summary>
        /// <param name="obj">Значение</param>
        /// <param name="propName">Имя переменной</param>
        /// <param name="minValue">Минимально возможное знчение</param>
        public static void ThrowIfValueLessThen(long obj, string propName, long minValue)
        {
            if (obj < minValue)
            {
                throw new DomainException($"Значение {propName} не должно быть меньше {minValue}");
            }
        }

        /// <summary>
        /// Кинуть исключение если значение меньше минимально возможного
        /// </summary>
        /// <param name="obj">Значение</param>
        /// <param name="propName">Имя переменной</param>
        /// <param name="minValue">Минимально возможное знчение</param>
        public static void ThrowIfValueLessThen(int obj, string propName, int minValue)
        {
            if (obj < minValue)
            {
                throw new DomainException($"Значение {propName} не должно быть меньше {minValue}");
            }
        }
    }
}
