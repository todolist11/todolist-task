﻿namespace TodoList.Task.Domain.Abstractions
{
    /// <summary>
    /// Интерфейс корня агрегата
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
