﻿namespace TodoList.Task.Domain.Abstractions
{
    /// <summary>
    /// Интерфейс UnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Сохранить изменения
        /// </summary>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns></returns>
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Сохранить сущности
        /// </summary>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns></returns>
        public Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
    }
}
