﻿namespace TodoList.Task.Domain.Abstractions
{
    /// <summary>
    /// Интерфейс Repository c UnitOfWork
    /// </summary>
    /// <typeparam name="T"><see cref="IAggregateRoot"/></typeparam>
    public interface IRepository<T>
        where T : IAggregateRoot
    {
        /// <summary>
        /// Точка работы (контекст)
        /// </summary>
        public IUnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns>Добавленная сущность</returns>
        public T Add(T entity);

        /// <summary>
        /// Обновить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        public void Update(T entity);

        /// <summary>
        /// Найти сущность по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <param name="userId"></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns>Сущность</returns>
        public Task<T> GetAsync(int id, int userId, CancellationToken cancellationToken = default);
    }
}
