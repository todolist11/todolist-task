﻿using TodoList.Task.Domain.Abstractions;

namespace TodoList.Task.Domain.Entities.TaskAggregate
{
    /// <summary>
    /// Интерфейс репозитория Задач
    /// </summary>
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
