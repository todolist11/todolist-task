﻿using System.Xml.Linq;
using TodoList.Task.Domain.Abstractions;

namespace TodoList.Task.Domain.Entities.TaskAggregate
{
    /// <summary>
    /// Задача
    /// </summary>
    public class Task : BaseEntity, IAggregateRoot
    {
        private readonly List<SubTask> _subTasks = new();

        /// <summary>
        /// Название задачи
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Срок выполнения
        /// </summary>
        public DateTime EndDate { get; private set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        public Enums.TaskStatus Status { get; private set; } = Enums.TaskStatus.InProgress;

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; private set; }

        /// <summary>
        /// Колекция подзадач задачи
        /// </summary>
        public ICollection<SubTask> SubTasks => _subTasks;

        public Task(
            string title,
            string description,
            DateTime endDate,
            Enums.TaskStatus status,
            int userId)
        {
            Guard.ThrowIfNullOrWhiteSpace(title, nameof(title));
            Guard.ThrowIfNull<DateTime>(endDate, nameof(endDate));
            Guard.ThrowIfNull<Enums.TaskStatus>(status, nameof(status));
            Guard.ThrowIfNull<int>(userId, nameof(userId));
            Guard.ThrowIfLengthExceeded(title, nameof(title), 200);
            Guard.ThrowIfLengthExceeded(description, nameof(description), 600);

            Title = title;
            Description = description;
            EndDate = endDate;
            Status = status;
            UserId = userId;
        }

        protected Task()
        {
        }
    }
}
