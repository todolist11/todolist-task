﻿using TodoList.Task.Domain.Abstractions;

namespace TodoList.Task.Domain.Entities.TaskAggregate
{
    /// <summary>
    /// Подзадача
    /// </summary>
    public class SubTask : BaseEntity
    {
        /// <summary>
        /// Название подзадачи
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Статус подзадачи
        /// </summary>
        public Enums.TaskStatus Status { get; private set; } = Enums.TaskStatus.InProgress;

        /// <summary>
        /// Внешний ключ задачи
        /// </summary>
        public int TaskId { get; private set; }

        /// <summary>
        /// <see cref="Task"/>
        /// </summary>
        public Task Task { get; private set; }

        public SubTask(
            string title,
            string description,
            Enums.TaskStatus status,
            int taskId)
        {
            Guard.ThrowIfNullOrWhiteSpace(title, nameof(title));
            Guard.ThrowIfNull<Enums.TaskStatus>(status, nameof(status));
            Guard.ThrowIfNull<int>(taskId, nameof(taskId));
            Guard.ThrowIfLengthExceeded(title, nameof(title), 200);
            Guard.ThrowIfLengthExceeded(description, nameof(description), 600);

            Title = title;
            Description = description;
            Status = status;
            TaskId = taskId;
        }

        protected SubTask()
        {
        }
    }
}
