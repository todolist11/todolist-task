﻿using System.Runtime.Serialization;

namespace TodoList.Task.Domain.Enums
{
    /// <summary>
    /// Статус заявки
    /// </summary>
    public enum TaskStatus
    {
        /// <summary>
        /// Невыполнен
        /// </summary>
        [EnumMember(Value = @"INC")]
        InComplete = -1,

        /// <summary>
        /// Выполняется
        /// </summary>
        [EnumMember(Value = @"INP")]
        InProgress = 0,

        /// <summary>
        /// Выполнено
        /// </summary>
        [EnumMember(Value = @"COMP")]
        Complete = 1,
    }
}
